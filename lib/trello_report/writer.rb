module TrelloReport
  class Writer
    extend HandyConst

    attr_reader :config, :tasks

    const :header, ['Последнее изменение', 'Исполнитель', 'Название задачи',
      'Адрес задачи в трелло', 'Статус (готовность в %)', 'Число потраченных часов']
    const :task_columns, [:last_activity, :worker, :name, :url, :status, :spent_time]

    def initialize config, tasks
      @config = config
      @tasks = tasks
      @row = 1
    end

    def write!
      worksheet.title = 'Report'
      write_header
      tasks.group_by(&:worker).each do |_, tasks|
        tasks.each { |task| write_task task }
        write_total tasks
        write_line
      end
      save
    end

    private

    def write_line columns = nil
      Array.wrap(columns).each_with_index do |column, index|
        worksheet[@row, index + 1] = column
      end
      @row += 1
    end

    def write_header
      write_line header
    end

    def write_task task
      columns = task_columns.map { |column| task.public_send column }
      write_line columns
    end

    def write_total tasks
      total_spent = tasks.sum &:spent_time
      write_line [''] * (header.length - 1) + ["Всего: #{total_spent}"]
    end

    def save
      worksheet.save
      session.collection_by_title(config[:google][:folder]).add spreadsheet
      session.root_collection.remove spreadsheet
    end

    def session
      @session ||= GoogleDrive.login config[:google][:email], config[:google][:password]
    end

    def spreadsheet
      @spreadsheet ||= session.create_spreadsheet name
    end

    def worksheet
      @worksheet ||= spreadsheet.worksheets.first
    end

    def name
      @name ||= "Report_#{Time.now.strftime('%d.%m.%y')}_#{SecureRandom.hex(2)}"
    end
  end
end
