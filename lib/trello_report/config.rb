module TrelloReport
  class Config
    attr_reader :config

    def initialize path
      @config = YAML.load_file(path).deep_symbolize_keys
    end

    delegate :[], to: :config
  end
end
