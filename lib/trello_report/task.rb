module TrelloReport
  class Task
    attr_reader :name, :desc, :last_activity_date, :last_activity, :url, :worker

    def initialize card
      extract_info card
    end

    def has_info?
      [worker, status, spent_time].all? &:present?
    end

    def status
      match = @full_name.match /\((\d+)%/
      return if match.nil?
      match[1].to_i
    end

    def spent_time
      match = @full_name.match(/(\d+) час/)
      return if match.nil?
      match[1].to_i
    end

    private

    def extract_info card
      @full_name = card.name
      @name = @full_name.sub(/\((.*?)\)$/, '')
      @desc = card.desc
      @last_activity_date = card.last_activity_date
      @last_activity = @last_activity_date.strftime('%d.%m.%y %H:%M')
      @url = card.short_url
      @worker = WorkersManager.get_or_load(card.member_ids.first).full_name if card.member_ids.any?
    end
  end
end
