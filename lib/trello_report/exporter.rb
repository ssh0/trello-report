module TrelloReport
  class Exporter
    attr_reader :config

    def initialize config
      @config = config
    end

    def export!
      auth!
      board = Trello::Board.find config[:trello][:board_id]
      board.cards.map { |card| Task.new card }
    end

    private

    def auth!
      Trello.configure do |option|
        option.developer_public_key = config[:trello][:pub_key]
        option.member_token = config[:trello][:token]
      end
    end
  end
end
