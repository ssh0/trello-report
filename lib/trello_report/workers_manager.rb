module TrelloReport
  module WorkersManager
    def self.get_or_load member_id
      return storage[member_id] if storage[member_id]
      member = Trello::Member.find member_id
      storage[member_id] = member
    end

    private

    def self.storage
      @storage ||= {}
    end
  end
end
