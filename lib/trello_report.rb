require 'active_support/all'
require 'yaml'
require 'trello'
require 'google_drive'
require 'handy_const'

require 'trello_report/version'
require 'trello_report/config'
require 'trello_report/exporter'
require 'trello_report/task'
require 'trello_report/writer'
require 'trello_report/workers_manager'

module TrelloReport
  def self.export! config_path, start_date
    config = Config.new config_path
    tasks = Exporter.new(config).export!
    tasks.select! { |task| task.has_info? and task.last_activity_date > start_date }
    Writer.new(config, tasks).write!
    true
  end
end
