lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'trello_report/version'

Gem::Specification.new do |spec|
  spec.name          = "trello_report"
  spec.version       = TrelloReport::VERSION
  spec.authors       = ["AK", "miraks"]
  spec.email         = ["asgard.ssh@gmail.com", "a@vldkn.net"]
  spec.summary       = %q{Trello report}
  spec.description   = %q{Trello report system}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "pry"
  spec.add_dependency "ruby-trello"
  spec.add_dependency "google_drive"
  spec.add_dependency "handy_const"
  spec.add_dependency "activesupport"
end
